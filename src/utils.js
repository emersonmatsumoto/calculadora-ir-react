import moment from 'moment'

export const formatCurrency = value => Number(value).toLocaleString('pt-BR', {minimumFractionDigits: 2, style: 'currency', currency: 'BRL' })

export const formatDate = value => moment(value).format('DD/MM/YYYY')
