import React from "react";
import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader
} from "@material-ui/core";
import {
  LocalAtm,
  CheckCircle,
  Warning,
  Description,
  StoreMallDirectory,
  TrendingUpRounded,
  MonetizationOnRounded,
} from "@material-ui/icons";
import { Link } from "react-router-dom";

function ListItemLink(props) {
  const { icon, primary, to } = props;

  const renderLink = React.useMemo(
    () =>
      React.forwardRef((linkProps, ref) => (
        <Link to={to} {...linkProps} innerRef={ref} />
      )),
    [to]
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
}

export default function Menu() {
  return (
    <List>
      <ListSubheader inset>Carteira</ListSubheader>
      <ListItemLink
        icon={<TrendingUpRounded />}
        primary="Ações"
        to="/acoes"
      />
       <ListItemLink
        icon={<Description />}
        primary="Tesouro direto"
        to="/tesouro-direto"
      />
       <ListItemLink
        icon={<StoreMallDirectory />}
        primary="FII"
        to="/fii"
      />

      <Divider />
      <ListSubheader inset>Imposto de Renda</ListSubheader>
      <ListItemLink
        icon={<Warning />}
        primary="Rend. tributáveis"
        to="/rendimentos-tributaveis"
      />
      <ListItemLink
        icon={<CheckCircle />}
        primary="Rend. isentos"
        to="/rendimentos-isentos"
      />
      <ListItemLink
        icon={<LocalAtm />}
        primary="Bens e Direito"
        to="/bens-e-direito"
      />
    </List>
  );
}
