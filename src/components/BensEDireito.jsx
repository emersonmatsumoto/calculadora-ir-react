import React from 'react'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';
import NumberFormat from 'react-number-format'


function createData(item, cod, localizacao, discriminacao, situacaoAnterior, situacao) {
  return { item, cod, localizacao, discriminacao, situacaoAnterior, situacao };
}

const rows = [
  createData('12', '31', '105 Brasil', '100 ações ABEV3 bla bla ball', 344234.0, 5.9),
  createData('12', '31', '105 Brasil', '100 ações ABEV3 bla bla ball', 4.0, 5.9),
  createData('12', '31', '105 Brasil', '100 ações ABEV3 bla bla ball', 4.0, 5.9),
  createData('12', '31', '105 Brasil', '100 ações ABEV3 bla bla ball', 4.0, 5.9),
  createData('12', '31', '105 Brasil', '100 ações ABEV3 bla bla ball', 4.0, 5.9),

];

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

const StyledNumberFormat = ({ value }) =>
  (<NumberFormat value={value} displayType={'text'} decimalScale={2} fixedDecimalScale={true} decimalSeparator={','} />)

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
}));

export default function BensEDireito() {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <StyledTableCell>Item</StyledTableCell>
            <StyledTableCell>Cod.</StyledTableCell>
            <StyledTableCell>Localização</StyledTableCell>
            <StyledTableCell>Discriminação</StyledTableCell>
            <StyledTableCell align="right">Situação em R$</StyledTableCell>
            <StyledTableCell align="right">Situação em R$</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <StyledTableRow>
              <StyledTableCell>{row.item}</StyledTableCell>
              <StyledTableCell>{row.cod}</StyledTableCell>
              <StyledTableCell>{row.localizacao}</StyledTableCell>
              <StyledTableCell>{row.discriminacao}</StyledTableCell>
              <StyledTableCell align="right"><StyledNumberFormat value={row.situacaoAnterior} /></StyledTableCell>
              <StyledTableCell align="right"><StyledNumberFormat value={row.situacao} /></StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  )
}
