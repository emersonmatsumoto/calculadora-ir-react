import React from 'react'
import MaterialTable from 'material-table'
import { formatCurrency, formatDate } from '../utils'

export default function ProventosRecebidos() {
  const [state, setState] = React.useState({
    columns: [
      {
        title: 'Tipo',
        field: 'tipo',
        lookup: { 10: 'Dividendos', 13: 'JCP' },
      },
      { title: 'Cód. Ação', field: 'codAcao' },
      { title: 'Qtde', field: 'qtde' },
      { title: 'Valor Bruto', field: 'valorBruto',  render: rowData => formatCurrency(rowData.valorBruto) },
      { title: 'IR', field: 'ir', render: rowData => formatCurrency(rowData.ir) },
      { title: 'Valor Líquido', field: 'valorLiquido',  render: rowData => formatCurrency(rowData.valorLiquido) },
      { title: 'Previsão Pagto', field: 'previsaoPagto', render: rowData => formatDate(rowData.previsaoPagto) },
    ],
    data: [],
  });

  return (
    <MaterialTable
      title="Editable Example"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              const data = [...state.data];
              data.push(newData);
              setState({ ...state, data });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              const data = [...state.data];
              data[data.indexOf(oldData)] = newData;
              setState({ ...state, data });
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              const data = [...state.data];
              data.splice(data.indexOf(oldData), 1);
              setState({ ...state, data });
            }, 600);
          }),
      }}
    />
  );
}
