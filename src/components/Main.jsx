import React from "react";
import { Switch, Route } from "react-router-dom";

import Home from "./Home";
import BensEDireito from "./BensEDireito";
import ProventosRecebidos from "./ProventosRecebidos";

export default function Main() {
  return (
    <Switch>
      <Route path="/bens-e-direito">
        <BensEDireito />
      </Route>
      <Route path="/proventos-recebidos">
        <ProventosRecebidos />
      </Route>
      <Route path="/">
        <Home />
      </Route>
    </Switch>
  );
}
