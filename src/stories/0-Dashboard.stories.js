import React from 'react';
import { linkTo } from '@storybook/addon-links';
import { Welcome } from '@storybook/react/demo';
import Dashboard from '../components/Dashboard'


export default {
  title: 'Dashboard',
};

export const toStorybook = () => <Dashboard showApp={linkTo('Button')} />;

toStorybook.story = {
  name: 'Home',
};
