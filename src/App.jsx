import React from "react";
import { withAuthenticator } from "aws-amplify-react";
import Amplify from "aws-amplify";

import Dashboard from "./components/Dashboard";

Amplify.configure({
  Auth: {
    region: "us-east-1",
    userPoolId: "us-east-1_BoJKVbbtH",
    userPoolWebClientId: "b7r0k41i374irjvsprmt3i6qo"
  }
});

const App = () => {
  return <Dashboard />;
};

export default withAuthenticator(App, {
  signUpConfig: {
    hiddenDefaults: ["phone_number"]
  }
});
